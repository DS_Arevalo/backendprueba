const express = require('express')

const routes = require('./routes')

const app = express()

var config = require('./config');


app.set('port', config.port)

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(express.json())

// routes
app.use('/api', routes)

// server
app.listen(app.get('port'), ()=>{
    console.log('server running on port', app.get('port'))
})