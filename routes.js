const express = require('express')
const routes = express.Router()
const fetch = require('node-fetch');
const config = require('./config');


routes.get('/movies', (req, res) => {
    req.query.filter != null && req.query.filter != 'null' ? filter = req.query.filter : filter = "now_playing"
    req.query.page != null && req.query.page != 'null' ? page = req.query.page : page = 1
    req.query.name != null && req.query.name != 'null' ? url = `${config.baseURL}search/movie?api_key=${config.apiKey}&query=${req.query.name}&page=${page}` : url = `${config.baseURL}movie/${filter}?api_key=${config.apiKey}&page=${page}`
    fetch(url)
        .then(res => res.json())
        .then(json => {
            res.json(json)
        })
        .catch(function(err) {
            console.log(err);
        });

})

routes.get('/movies/:id', (req, res) => {
    let url = `${config.baseURL}movie/${req.params.id}?api_key=${config.apiKey}`
    fetch(url)
        .then(res => res.json())
        .then(json => {
            res.json(json)
        })
        .catch(function(err) {
            console.log(err);
        });
})

module.exports = routes